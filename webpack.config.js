/*** webpack.config.js ***/
const HtmlWebpackPlugin = require("html-webpack-plugin");
const htmlWebpackPlugin = new HtmlWebpackPlugin({
    template: "./public/index.html",
    favicon: "./public/favicon.ico",
    filename: "./index.html"
});
module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "bundle.js",
        publicPath: ''
    },
    performance: {
        hints: process.env.NODE_ENV === 'production' ? "warning" : false
      },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.(s*)css$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                use: {
                  loader: "file-loader",
                  options: {
                    name: "fonts/[name].[ext]",
                  },
                }
            },
            {
                test: /\.(svg|png|jpeg|jpg)$/,
                use: {
                  loader: "file-loader",
                  options: {
                    name: "imgs/[name].[ext]",
                  },
                }
            }
        ]
    },
    plugins: [htmlWebpackPlugin],
    resolve: {
        extensions: [".js", ".jsx"]
    },
    devServer: {
        historyApiFallback: true,
        port: 4200
    }
};