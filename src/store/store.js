import thunk from "redux-thunk";
import rootReducer from '../core/reducers';
import { applyMiddleware, createStore, compose } from "redux";

const initialState = {};

const middlewares = [thunk];

const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares),    
)

export default store;