import axios from 'axios';

const fetchMovieURL = "http://www.omdbapi.com/?apikey=e259e07&type=movie&plot=short&page=1";
const fetchMovieURLDefault = "http://www.omdbapi.com/?apikey=e259e07&type=movie&page=1&s=fast";

export default class AxiosService {

    static findByTitle(title) {
        title = title.replace("&", "%26"); //Por algum motivo ele não entende corretamente o caractére &
        return axios.get(fetchMovieURL + `&t=${title}`);
    }

    static find(search) {
        if (search) {
            return axios.get(fetchMovieURL + `&s=${search}`);
        } else {
            return axios.get(fetchMovieURLDefault);
        }
    }
}

