import React from 'react';
import { toast } from 'react-toastify';

const toastContainer = (toastFields) => {
    return (
        <div>
            <strong>
                {toastFields.title}
            </strong>
            <br />
            <i>{toastFields.body}</i>
        </div>
    );
}

export const ToastTrigger = (code, err) => {
    let toastFields = ToastHandler(code, err);    

    toast[toastFields.type](toastContainer(toastFields), {
        position: "top-right",
        autoClose: 7000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
    });
}

export const ERROR_CODES = {
    ConnectionFailed: 100,
    RequestTimeout: 124,
    screenDimensionGetError: 666, 
    printError: 333   
}

//Error Type: success, error, default, warning, info

//Usualmente coloco numa pasta chamada Shared, neste projeto não coloquei para não poluir muito, devido a este ser o único arquivo na pasta.
const ToastHandler = (code, error) => {
    switch (code) {
        case ("success"):
            return {
                type: 'success',
                title: 'Operação realizada com sucesso!',
                body: ''
            };
        case(ERROR_CODES.printError):
            return {
                type: 'error',
                title: 'Erro ao obter a lista de Filmes!',
                body: `O servidor respondeu com: "${error}"`
            };
        case (ERROR_CODES.ConnectionFailed):
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "Não foi possível obter as dimensões da tela!"
            }; 
        case (ERROR_CODES.ConnectionFailed):
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "Não foi possível obter as dimensões da tela!"
            }; 
        case (ERROR_CODES.ConnectionFailed):
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "Não foi possível conectar com o servidor! Recarregue a página para tentar novamente!"
            };
        case (ERROR_CODES.RequestTimeout):
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "O servidor demorou muito para responder!"
            };
        default:
            return {
                type: 'error',
                title: 'Ops, Error!',
                body: "Ocorreu um erro ao realizar a operação, por favor contate o administrador do sistema."
            };
    }
}
