const actionTypes = {
    SET_SCREEN_SIZE: "SET_SCREEN_SIZE",
    IS_SEARCHING: "IS_SEACHRING",
    RETRIE_MOVIE_DATA: "RETRIEVE_MOVIE_DATA",
    RETRIEVE_UNIQUE_MOVIE: "RETRIEVE_UNIQUE_MOVIE",
    LOADING: "LOADING"
}

export default actionTypes;