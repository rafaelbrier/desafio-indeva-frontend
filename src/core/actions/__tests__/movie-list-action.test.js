import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import mockData from '../__mocks__/mockData';
import {
  showLoading,
  hideLoading,
  retrieveMovieListAction,
  retrieveUniqueMovieByTitle,
  isSearchingAction
} from '../movie-list-action';
import actionTypes from '../actionTypes';

const middlewares = [thunk]
const mockStore = configureStore(middlewares);
const store = mockStore({});

describe('Movie List Actions', () => {
  beforeEach(() => {
    store.clearActions();
  });


  test('verify if Show Loading action is working properly', () => {
    const expectedActions = [{
        type: actionTypes.LOADING,
        payload: true
      }];
    store.dispatch(showLoading())
    expect(store.getActions()).toEqual(expectedActions);
  });


  test('verify if Hide Loading action is working properly', () => {
    const expectedActions = [{
        type: actionTypes.LOADING,
        payload: false
      }];
    store.dispatch(hideLoading())
    expect(store.getActions()).toEqual(expectedActions);
  });


  test('verifiy if first API request is satisfied correctly', () => {
    const { fastSearchMovieList } = mockData;

    const expectedActions = [
      { type: actionTypes.LOADING, payload: true },
      { type: actionTypes.RETRIE_MOVIE_DATA, payload: fastSearchMovieList.Search, error: null },
      { type: actionTypes.LOADING, payload: false }
    ];

    return store.dispatch(retrieveMovieListAction("Fast"))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });


  test('verifiy if Error Handler of the API is working properly', () => {
    const { toManyResults } = mockData;

    const expectedActions = [
      { type: actionTypes.LOADING, payload: true },
      { type: actionTypes.RETRIE_MOVIE_DATA, error: toManyResults.Error },
      { type: actionTypes.LOADING, payload: false }
    ];

    return store.dispatch(retrieveMovieListAction("The"))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });


  test('verify if first MovieInfo request is satisfied correctly', () => {
    const { batmanUniqueMovieInfo } = mockData;

    const expectedActions = [
      { type: actionTypes.LOADING, payload: true },
      { type: actionTypes.RETRIEVE_UNIQUE_MOVIE, payload: batmanUniqueMovieInfo, wasRedirect: true },
      { type: actionTypes.LOADING, payload: false }
    ];

    return store.dispatch(retrieveUniqueMovieByTitle("Batman"))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });


  test('verify if Mobile/Browser action identifies Device correctly', () => {
    const expectedActions = [
      { type: actionTypes.IS_SEARCHING, payload: true },
      { type: actionTypes.IS_SEARCHING, payload: false }
    ];

    store.dispatch(isSearchingAction("MOBILE"));
    store.dispatch(isSearchingAction(""));

    expect(store.getActions()).toEqual(expectedActions);
  });
})