import actionTypes from "./actionTypes";
import {ToastTrigger, ERROR_CODES} from "../services/toast-service";

export const getScreenSizeAction = (values) => (dispatch) => {
    if (values && values.width && values.height) {
        dispatch({
            type: actionTypes.SET_SCREEN_SIZE,
            payload: {
                width: values.width,
                height: values.height
            }
        });
    } else {
        ToastTrigger(ERROR_CODES.screenDimensionGetError);
    }
}