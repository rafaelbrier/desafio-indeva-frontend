import AxiosService from "../services/axios-service";
import { ERROR_CODES, ToastTrigger } from "../services/toast-service";
import actionTypes from "./actionTypes";

export const showLoading = () => {
    return {
        type: actionTypes.LOADING,
        payload: true
    }
}
export const hideLoading = () => {
    return {
        type: actionTypes.LOADING,
        payload: false
    }
}

export const retrieveMovieListAction = (searchValue = null) => {

    return dispatch => { 

        dispatch(showLoading());

        return AxiosService.find(searchValue || "Fast")
            .then((res) => {
                if (!res.data.Error) {                 
                    dispatch({
                        type: actionTypes.RETRIE_MOVIE_DATA,
                        payload: res.data.Search,
                        error: null
                    });
                } else {
                    dispatch({
                        type: actionTypes.RETRIE_MOVIE_DATA,
                        error: res.data.Error
                    });
                }

                dispatch(hideLoading());
               
            }, err => {
                ToastTrigger(err.code);
                
                dispatch(hideLoading());
            })
    }
}

export const retrieveUniqueMovieByTitle = (title = null) => {

    return dispatch => {

        dispatch(showLoading());

        return AxiosService.findByTitle(title)
            .then((res) => {
                if (res.data.Error) {                    
                    ToastTrigger(ERROR_CODES.printError, res.data.Error)
                } else {
                    dispatch({
                        type: actionTypes.RETRIEVE_UNIQUE_MOVIE,
                        payload: res.data,
                        wasRedirect: true
                    });
                }

                dispatch(hideLoading());

            }, err => {
                ToastTrigger(err.code);

                dispatch(hideLoading());

            })
    }
}

export const isSearchingAction = (device) => (dispatch) => {
    if (device === "MOBILE") {
        dispatch({
            type: actionTypes.IS_SEARCHING,
            payload: true
        });
    } else {
        dispatch({
            type: actionTypes.IS_SEARCHING,
            payload: false
        });
    }
}


export const stopSearchAction = () => (dispatch) => {
    dispatch({
        type: actionTypes.IS_SEARCHING,
        isSearching: false
    })
}
