import React from 'react';
import { shallow } from 'enzyme';
import MovieCard from '../movie-card/movie-card';

describe('MovieCard', () => {
    it('should render correctly', () => {
        const wrapper = shallow(<MovieCard />);

        expect(wrapper.length).toEqual(1);
    });


    it('should handle and exhibit correctly the received props', () => {
        const moviePoster = "Jest Poster Test";
        const movieTitle = "Jest Title Test";
        const movieYear = "9999";

        const wrapper = shallow(<MovieCard moviePoster={moviePoster} movieTitle={movieTitle} movieYear={movieYear} />);

        const image = wrapper.find("img");
        const title = wrapper.find("h2").text();
        const year = wrapper.find("span").text();

        expect(image.prop("src")).toEqual(moviePoster);
        expect(title).toEqual(movieTitle);
        expect(movieYear).toEqual(movieYear);
    });
});