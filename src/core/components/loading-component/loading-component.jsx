import React, { Component } from 'react';
import './loading-component.scss';

export default class LoadingComponent extends Component {

    render() {
        return (
            <div>
                <div className="light-color">Aguarde...</div>
                <div className="lds-spinner">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        )
    }
}