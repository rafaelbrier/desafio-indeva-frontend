import React, { Component } from 'react';

export default class If extends Component {

    render() {
        let elementToDisplay = this.props.condition
        ? (this.props.children ? this.props.children : "")
        : null;
        return elementToDisplay;
    }
}