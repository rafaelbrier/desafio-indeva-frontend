import React, { Component } from 'react';
import './movie-card.scss';

export default class MovieCard extends Component {

    render() {
        let moviePoster = this.props.moviePoster
            ? this.props.moviePoster
            : "https://images.unsplash.com/photo-1516541196182-6bdb0516ed27?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ccdbaaf7bb352c17aaf967cf09c1285c&w=1000&q=80"
        return (
            <div className="movie-card-container" onClick={this.props.onClick}>
                <div className="movie-card-subcontainer">
                    <img src={moviePoster} alt="Movie poster" />
                    <div className="overlay-content"></div>
                    <h2 className="light-color">{this.props.movieTitle}</h2>
                    <span className="light-color">{this.props.movieYear}</span>
                </div>
            </div>
        )
    }
}