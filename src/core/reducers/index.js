import { combineReducers } from "redux";
import screenSizeReducer from './screen-size-reducer';
import movieListReducer from './movie-list-reducer';

const rootReducer = combineReducers({
    screenSizeReducer,
    movieListReducer
})

export default rootReducer;