import actionTypes from '../actions/actionTypes';

const initialState = {
   screen: {width:0, height:0},
}

const screenSizeReducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTypes.SET_SCREEN_SIZE:
            return {
                ...state,
                screen: action.payload 
            };

        default:
            return state;
    }
}

export default screenSizeReducer