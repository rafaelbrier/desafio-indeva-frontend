import actionTypes from '../actions/actionTypes';

const initialState = {
    movieData: [],
    movieInfo: {},
    error: undefined,
    isSearching: false,
    wasRedirect: false,
    isLoading: false,
}

const movieListReducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTypes.IS_SEARCHING:
            return {
                ...state,
                isSearching: action.payload
            };

        case actionTypes.RETRIE_MOVIE_DATA:
            return {
                ...state,
                movieData: action.payload,
                error: action.error,
            };

        case actionTypes.RETRIEVE_UNIQUE_MOVIE:
            return {
                ...state,
                movieInfo: action.payload,
                wasRedirect: action.wasRedirect
            };      
        case actionTypes.LOADING:
            return {
                ...state,
                isLoading: action.payload
            };
        default:
            return state;
    }
}

export default movieListReducer;