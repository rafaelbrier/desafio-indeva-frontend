import actionTypes from '../../actions/actionTypes';
import movieListReducer from '../movie-list-reducer';
import screenSizeReducer from '../screen-size-reducer';

const initialStateMovieList = {
    movieData: [],
    movieInfo: {},
    error: undefined,
    isSearching: false,
    wasRedirect: false,
    isLoading: false,
}

const initialStateScreenSize = {
    screen: { width: 0, height: 0 },
}

describe('Movie List Reducer', () => {

    it('should have initial state', () => {
        expect(movieListReducer(undefined, {})).toEqual(initialStateMovieList);
    });

    it('should return searching state', () => {
        const startAction = {
            type: actionTypes.IS_SEARCHING,
            payload: true
          };
          const mustReturn = {
            ...initialStateMovieList, 
            isSearching: true
          }
        expect(movieListReducer(undefined, startAction)).toEqual(mustReturn);
    });

    it('should return correctly movie data initially', () => {
        const startAction = {
            type: actionTypes.RETRIE_MOVIE_DATA,
            payload: "Fast",
            error: "Movie not Found"
          };
        const mustReturn = {
            ...initialStateMovieList,
            movieData: "Fast",
            error: "Movie not Found"
        };
        expect(movieListReducer(undefined, startAction)).toEqual(mustReturn);
    });

    it('should return correctly loading state', () => {
        const startAction = {
            type: actionTypes.LOADING,
            payload: false,
          };
        const mustReturn = {
            ...initialStateMovieList,
            isLoading: false
        };
        expect(movieListReducer(undefined, startAction)).toEqual(mustReturn);
    });
});



describe('Screen Size Reducer', () => {

    it('should have initial state', () => {
        expect(screenSizeReducer(undefined, {})).toEqual(initialStateScreenSize);
    });

    it('should return correctly screen size', () => {
        const startAction = {
            type: actionTypes.SET_SCREEN_SIZE,
            payload: 499,
          };
        const mustReturn = {
            ...initialStateScreenSize,
            screen: 499
        };
        expect(screenSizeReducer(undefined, startAction)).toEqual(mustReturn);
    });

});