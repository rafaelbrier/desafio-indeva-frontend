import React, { Component } from 'react';
import './movie-list-page.scss';
import { connect } from 'react-redux';
import { isSearchingAction, retrieveMovieListAction, retrieveUniqueMovieByTitle, stopSearchAction } from '../../core/actions/movie-list-action';
import If from '../../core/components/if-component';
import MovieCard from '../../core/components/movie-card/movie-card';
import LoadingComponent from '../../core/components/loading-component/loading-component';

class MovieListPage extends Component {

    searchPlaceholder = "Que filme você procura?";
    deviceRunning: string;
    isSearching = false;
    searchValue: string;
    titleInfoWeb: string;
    titleInfoMobile: string;

    componentWillMount() {
        this.titleInfoMobile = "Sugestões";
        this.titleInfoWeb = "Tendência";
        this.props.retrieveMovieListAction();
    }

    searchOnChange = (event) => {
        this.searchValue = event.target.value;
        this.titleInfoWeb = this.searchValue || "Tendência";
        this.titleInfoMobile = this.searchValue || "Sugestões";
        this.props.retrieveMovieListAction(this.searchValue);
    }

    movieCardClick = (movieTitle) => {
        this.props.retrieveUniqueMovieByTitle(movieTitle)
            .then(() => {
                this.props.history.push({
                    pathname: '/movie-info',
                    search: `?title=${movieTitle}`,
                });
            });
    }

    searchMobileOnBlur = () => {
        if (this.searchValue === "" || this.searchValue === "Tendência") {
            this.resetButtonClick();
        }
    }

    searchMobileButtonClick = () => {
        this.props.isSearchingAction(this.runningAt());
    }

    resetButtonClick = () => {
        this.searchValue = null;
        this.props.retrieveMovieListAction();
        this.props.stopSearchAction();
    }

    listClick = (e) => {
        this.searchValue = e.currentTarget.dataset.id;
        this.props.retrieveMovieListAction(this.searchValue);
    }

    runningAt = () => {
        let runningNow = this.deviceRunning;
        if (this.props.screen.width < 499) {
            this.deviceRunning = "MOBILE";
        } else if (this.props.screen.width < 1000 && this.props.screen.width > 500) {
            this.deviceRunning = "TABLET";
        } else {
            this.deviceRunning = "BROWSER";
        }
        this.isSearching = this.deviceRunning !== runningNow ? false : this.isSearching;

        return this.deviceRunning;
    }

    render() {
        this.isSearching = this.props.search.isSearching;
        this.runningAt();
        let movieData = this.props.search.movieData;
        let error = this.props.search.error;
        let isLoading = this.props.search.isLoading;
        return (
            <div>
                <If condition={!this.isSearching}>
                    <div className="mlist-header">
                        <img src={require('../../assets/images/movie-on-logo.svg')} className="mlist-logo" />


                        <span className="mlist-header-subcontainer">
                            <span className="ui icon input search-browser">
                                <input placeholder={this.searchPlaceholder} type="text" name="searchValue" onChange={this.searchOnChange} className="mlist-search" />
                                <i aria-hidden="true" className="search icon light-color" />
                            </span>

                            <span className="mlist-profile">
                                <span className="search-mobile">
                                    <button className="ui icon button" role="button" onClick={this.searchMobileButtonClick}>
                                        <i aria-hidden="true" className="search icon light-color" />
                                    </button>
                                </span>
                                <a className="light-color" href="https://rafaelbrier.github.io/">
                                    <img src={require('../../assets/images/3x4.jpg')} />
                                    <span>Rafael Marcio Brier</span>
                                </a>
                            </span>
                        </span>
                    </div>
                </If>

                <If condition={this.isSearching}>
                    <div className="ui left icon input fluid input-search-mobile">
                        <input placeholder={this.searchPlaceholder} type="text" name="searchValue" onChange={this.searchOnChange}
                            onBlur={this.searchMobileOnBlur} />
                        <i aria-hidden="true" className="search icon" />
                        <button className="ui icon button" role="button" onClick={this.resetButtonClick}>
                            <i className="times icon light-color" />
                        </button>
                    </div>

                    <h1 className="mlist-title light-color">{this.titleInfoMobile}</h1>
                    <If condition={!this.searchValue}>
                        <li data-id="Batman" className="mlist-suggestion light-color" onClick={this.listClick}>Batman</li>
                        <li data-id="Avengers" className="mlist-suggestion light-color" onClick={this.listClick}>Avengers</li>
                        <li data-id="Iron Man" className="mlist-suggestion light-color" onClick={this.listClick}>Iron Man</li>
                    </If>
                </If>


                <If condition={!this.isSearching || this.searchValue}>
                    <If condition={!this.isSearching || this.deviceRunning !== "MOBILE"} >
                        <h1 className="mlist-title light-color">{this.titleInfoWeb}</h1>
                    </If>

                    <If condition={!error} >
                        {isLoading ?
                            <div className="center-loader center-div">
                                <LoadingComponent />
                            </div> :
                            <div className="mlist-card-container">
                                {movieData ?
                                    movieData.map((movie) => (
                                        <MovieCard key={movie.imdbID}
                                            moviePoster={movie.Poster}
                                            movieTitle={movie.Title}
                                            movieYear={movie.Year}
                                            onClick={() => this.movieCardClick(movie.Title)}
                                        />
                                    )) : ' '}
                            </div>}
                    </If>

                    <If condition={error}>
                        <div className="center-div" style={{top:"auto"}}>
                            <h1 className="ui header ">
                                <i className="exclamation icon light-color" />
                                <div className="content light-color">
                                    Erro
                                <div className="sub header light-color">"{error}"</div>
                                </div>
                            </h1>
                        </div>
                    </If>
                </If>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        screen: state.screenSizeReducer.screen,
        search: state.movieListReducer,
    };
}

const mapDispatchToProps = {
    isSearchingAction,
    stopSearchAction,
    retrieveMovieListAction,
    retrieveUniqueMovieByTitle
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieListPage);