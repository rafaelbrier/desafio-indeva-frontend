import React, { Component } from 'react';
import './movie-info-page.scss';
import { Redirect } from 'react-router-dom';
import Grid from 'semantic-ui-react/dist/es/collections/Grid/Grid';
import Image from 'semantic-ui-react/dist/es/elements/Image/Image';
import Button from 'semantic-ui-react/dist/es/elements/Button/Button';

import { connect } from 'react-redux';

import If from '../../core/components/if-component';

class MovieInfoPage extends Component {

    screenWidth: string;

    return = () => {
        this.props.history.goBack();
    }

    starRating = (rating) => {
        let movieRating = rating / 2;
        let tagArrays = [];
        for (let i = 1; i <= 5; i++) {
            if (i <= Math.floor(movieRating)) {
                tagArrays.push(<Image key={i} src={require('../../assets/images/filled-star.png')} />);
            } else if (!Number.isInteger(movieRating)) {
                movieRating = Math.round(movieRating);
                tagArrays.push(<Image key={i} src={require('../../assets/images/half-star.png')} />);
            } else {
                tagArrays.push(<Image key={i} src={require('../../assets/images/empty-star.png')} />);
            }
        }
        return tagArrays;
    }

    movieImg(movieInfo) {
        return <Image className="poster-img" src={movieInfo.Poster} />
    }
    movieContent(movieInfo) {
        return (
            <div className="info-content">
                <h1 className="light-color">{movieInfo.Title}
                    <p className="light-color shadow-text">{movieInfo.Year}  |  {movieInfo.Runtime}  |  {movieInfo.Genre}</p>
                </h1>
                <div className="score">

                    {this.starRating(movieInfo.imdbRating).map(star => star)}

                    <span className="light-color shadow-text">{movieInfo.imdbRating}/10</span>
                </div>
                <h3 className="light-color shadow-text">Sinopse</h3>
                <h4 className="light-color shadow-text">{movieInfo.Plot}</h4>
                <If condition={this.screenWidth < 499}>
                    <Button className="back-buttom light-color" fluid onClick={this.return}>Voltar</Button>
                </If>
            </div>
        );
    }


    render() {
        let movieInfo = this.props.movieInfo;
        this.screenWidth = this.props.screen.width;
        return (
            <div>
                {this.props.movieInfo && this.props.wasRedirect ?
                    <div className="content">
                        <If condition={this.screenWidth > 499} >
                            <Grid centered verticalAlign='middle' >
                                <Grid.Column className="poster-img-container" width={6}>
                                    {this.movieImg(movieInfo)}
                                </Grid.Column>
                                <button className="ui icon button back-times-buttom light-color"
                                    role="button" onClick={this.return}>
                                    <span>
                                        Voltar
                                        </span>
                                    <i className="times icon light-color" />
                                </button>
                                <Grid.Column width={10} className="change-mobile">
                                    {this.movieContent(movieInfo)}
                                </Grid.Column>
                            </Grid>
                        </If>
                        <If condition={this.screenWidth < 499} >
                            <div className="mobile-img-container">
                                {this.movieImg(movieInfo)}
                            </div>
                            <button className="ui icon button back-times-buttom light-color"
                                role="button" onClick={this.return}>
                                <i className="times icon light-color" />
                            </button>
                            {this.movieContent(movieInfo)}
                        </If>
                    </div>
                    : <Redirect to="/" />}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        screen: state.screenSizeReducer.screen,
        movieInfo: state.movieListReducer.movieInfo,
        wasRedirect: state.movieListReducer.wasRedirect,
    };
}


export default connect(mapStateToProps)(MovieInfoPage);