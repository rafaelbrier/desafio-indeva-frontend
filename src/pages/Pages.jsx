import React, { Component } from 'react';
import './Pages.scss'
import Routes from './routes';
import { BrowserRouter } from 'react-router-dom';

class Pages extends Component {
    render() {
        return (
            <BrowserRouter>
                <Routes />
            </BrowserRouter>
        );
    }
}

export default Pages;