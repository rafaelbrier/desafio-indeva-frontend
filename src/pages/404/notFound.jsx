import React, { Component } from 'react';
import Button from 'semantic-ui-react/dist/es/elements/Button/Button.js';
import Icon from 'semantic-ui-react/dist/es/elements/Icon/Icon';

export default class PageNotFound extends Component {
    constructor(props) {
        super(props);

        this.goBack = this.goBack.bind(this);
    }

    goBack() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="center-div" style={{top: "auto"}}>
                <h1 className="ui header ">
                    <i className="exclamation triangle icon main-color" style={{ fontSize: 50 }} />
                    <div className="content  main-color">
                        404
                     <div className="sub header  main-color">Oops! Página não encontrada.</div>
                        <Button icon labelPosition='left' className="ui mini inverted orange button" onClick={this.goBack}
                            style={{ position: "absolute", left: "35%", marginTop: "10%" }}>
                            <Icon name='angle left' />
                            Voltar
                        </Button>
                    </div>
                </h1>
            </div>
        );
    }
}