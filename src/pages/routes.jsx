import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import MovieListPage from './movie-list-page/movie-list-page';
import NotFound from './404/notFound';
import movieInfoPage from './movie-info-page/movie-info-page';


export default props => (
    <Switch>
        <Route exact path='/' component={MovieListPage} />
        <Route path='/movie-info' component={movieInfoPage} />
        <Route path='/not-found' component={NotFound} />
        <Route path='*' component={NotFound} />
    </Switch>
)