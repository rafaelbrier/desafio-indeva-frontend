import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import './App.scss';
import { getScreenSizeAction } from './core/actions/screen-size-action';
import Pages from './pages/Pages';

class App extends Component {

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
        this.props.getScreenSizeAction({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        return (
            <div>
                <ToastContainer toastClassName="toast-shadow" />
                <Pages />
            </div>
        );
    }
}

const mapDispatchToProps = {
    getScreenSizeAction
}
export default connect(null, mapDispatchToProps)(App);