# Desafio Indeva Frondend 

## Desafio realizado por [Rafael Marcio][rafaelmarcio] como solicitado pela da Indeva.


- Fiz uma parte do ajuste da Tela ao Browser e ao Mobile com apenas CSS e outra com JS, mostrando a possibilidade de se utilizar individualmente cada um. O sistema monitora qual dispositivo está rodando à aplicação para renderizar os componentes corretamente.

- Inclusive, ele monitora em tempo real o tamanho da tela, de forma que em dispositivos móveis seja possível rotacionar a tela para mudar a exibição.

- Coloquei para exibir na tela durante a pesquisa os erros que a API retorna, como "nenhum filme encontrado" e "muitos resultados".

- A tela com as informações de cada filme individual só é carregada se o sistema receber corretamente os dados. Também, o acesso a esta tela só é possível se for via redirecionamento, desta forma impeço o usuário de acessar a tela sem nenhum dado de filme carregado, o que seria um problema.

- Evitei criar pastas com arquivos únicos, mas em aplicações de grande parte prezo fortemente por modularidade.

- Foi utilizado o Semantic Ui nesta aplicação. Costumo usar bastante o Bootstrap também.

- Quanto ao git, usualmente uso uma Branch "Develop" para desenvolver, e posteriormente dou 'merge' com a "Master". Neste caso não utilizei este esquema por ser um teste pequeno.

- Foi escolhido o Jest para efetuar os testes devido a sua simplificidade. Para testar os componentes foi utilizado o Enzyme devido a sua já conhecida fácil aplicabilidade em React.


[rafaelmarcio]: https://rafaelbrier.github.io/
